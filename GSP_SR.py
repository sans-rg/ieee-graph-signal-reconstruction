# -*- coding: utf-8 -*-
"""
        Graph Signal Reconstruction techniques
"""
import numpy as np
import math
import pandas as pd
import numpy.linalg as LA

class GSR():
    """ This library contain the implementation different Graph Signal Reconstruction techniques"""


    def semi_supervised_interpolation(self, x, miss, L):
        '''
        Data reconstruction via Interpolated Regularization (Belkin'04)

        Parameters
        ----------
        x : array float (m)
            Set of m observed nodes.
        miss : array int (N-m)
            Set of missing nodes indexes.
        L : matrix float (NxN)
            Laplacian matrix.

        Raises
        ------
        Exception
            Framework has not been trained.

        Returns
        -------
        x_pred : array float (m)
            Reconstructed values for the m nodes.

        '''
        N = len(x) + len(miss)
        # Standardize observed nodes
        obs_indexes = np.delete(np.arange(N), miss)
        # Laplacian submatrices
        L_m = np.copy(L[miss, :])
        L_mm = np.copy(L_m[:, miss])
        # Perform reconstruction
        try:
            INV = LA.inv(L_mm)
        except:
            # If no solution return 0
            x_pred = np.zeros(len(miss))
            return x_pred
        
        x_pred = -INV@L_m[:, obs_indexes]@x
        return x_pred



    def diffussion_kernel(self, input, sigma_2):
        """ Applies diffusion kernel to input
        Parameters
        ----------
        input: int/list
            laplacian eigenvalue
        sigma_2: float
            kernel scale parameter
        Returns
        -------
        int/list:
            exp(sigma_2*input/2.0)
        """
        return np.exp((sigma_2*input)/2.0)

    def laplacian_kernel(self, input, sigma_2):
        """ Applies lasplacian kernel to input
        Parameters
        ----------
        input: int/list
            laplacian eigenvalue
        sigma_2: float
            kernel scale parameter
        Returns
        -------
        int/list:
            1 + sigma_2*input
        """
        return (1.0 + (sigma_2*input))

    def RW_kernel(self, input, P, a):
        """ Applies random walk kernel to input
        Parameters
        ----------
        input: int/list
            laplacian eigenvalue
        P: float
            kernel parameter
        a: float
            kernel parameter
        Returns
        -------
        int/list:
            (a - input)**(-P)
        """
        return (a - input)**(-P)

    def train_graph_KRR(self, K, y, SAMPLING, S, mu):
        """
        Training  graph kernel ridge regression from
         Romero et al.
        Parameters
        ----------
        K: matrix
            Kernel Matrix
        y: list
            Measurement vector
        SAMPLING: matrix
            Binary sampling matrix
        S: int
            Number of observed values
        mu: float
            Regularization paramerter
        Returns
        -------
        list:
            Reconstructed graph signal
        list:
            Alphas coefficients for reconstruction
        """
        K_ = SAMPLING@(K@SAMPLING.T)
        inverse = LA.inv(K_ + (mu*S*np.identity(S)))
        alphas = inverse@y
        prediction = SAMPLING.T@alphas
        prediction = K@prediction
        return prediction, alphas

    def predict_graph_KRR(self, k, alphas):
        """Prediction of a value given its kernel similarities and Alphas
        Parameters
        ----------
        k: list
            Kernel vector with other vertices
        alphas: list
            Vector of coefficients obtained in the training
        Returns
        -------
        pred: float
            Reconstructed value at node x
        """
        pred = k@alphas
        return pred

    def signal_reconstruction_low_pass(self, y, A, indices, k):
        """ Signal reconstruction given the k lowest varying frequencies
           from Stankovic et al.
        Parameters
        ----------
        y: list
            Measurement vector
        A : matrix
            Measurement Matrix, order from slow-varying to fast-varying
            eigenvectors
        indices: list
            List of observed nodes indexes
        k: int
            Number of lowest frequencies to keep
        Returns
        -------
        list:
            Rceonstructed graph signal
        """
        A_MK = A[indices, :k]
        N, _ = A.shape
        X_K = LA.pinv(A_MK)@y
        X_def = np.zeros(N)
        X_def[:k] = X_K
        x_def = A@X_def
        return x_def
