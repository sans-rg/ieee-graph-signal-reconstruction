# Graph signal reconstruction Python library #

This repository contains the file "GSP_SR.py" which is a Python library that contains the three methods for graph signal reconstruction used in the paper: "Graph Signal Reconstruction Techniques for IoT Air Pollution Monitoring Platforms" submitted to the IEEE Internet of Things Journal. More precisely, the implemented methods are; the graph-based interpolation from [1], a graph signal processing low-pass based model [2] and graph kernel ridge regression from [3]. Thus, these are methods from three families; graph-based semi-supervised learning, signal processing and kernel methods.


### Refrences ###

	[1] Belkin, M., Matveeva, I., & Niyogi, P. (2004, July). Regularization and semi-supervised learning on large graphs. In International Conference on Computational Learning Theory (pp. 624-638). Springer, Berlin, Heidelberg.
	[2] Stanković, L., Sejdić, E., Stanković, S., Daković, M., & Orović, I. (2019). A tutorial on sparse signal reconstruction and its applications in signal processing. Circuits, Systems, and Signal Processing, 38(3), 1206-1263.
	[3] Romero, D., Ma, M., & Giannakis, G. B. (2016). Kernel-based reconstruction of graph signals. IEEE Transactions on Signal Processing, 65(3), 764-778.
